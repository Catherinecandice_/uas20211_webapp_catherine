# Generated by Django 3.2 on 2022-01-04 07:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ActivityFilter',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('my_field', models.CharField(choices=[('running', 'RUNNING'), ('ride', 'RIDE'), ('cycling', 'CYCLING'), ('all', 'ALL')], default='running', max_length=7)),
            ],
        ),
        migrations.CreateModel(
            name='Clubs',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=300)),
                ('profile_medium', models.CharField(max_length=300)),
            ],
        ),
        migrations.CreateModel(
            name='Gender',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('my_field', models.CharField(choices=[('male', 'MALE'), ('female', 'FEMALE')], default='male', max_length=6)),
            ],
        ),
        migrations.CreateModel(
            name='Details',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('firstname', models.CharField(max_length=100)),
                ('lastname', models.CharField(max_length=100)),
                ('profile', models.CharField(max_length=300)),
                ('city', models.CharField(max_length=100)),
                ('state', models.CharField(max_length=100)),
                ('created_at', models.CharField(max_length=100)),
                ('chosen_clubs', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='API.clubs')),
                ('gender_choice', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='API.gender')),
            ],
        ),
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(max_length=100)),
                ('activity_choice', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='API.activityfilter')),
            ],
        ),
    ]
