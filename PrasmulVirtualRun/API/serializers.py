from django.contrib.auth import models
from django.contrib.auth.models import User
from API.models import Activity, ActivityFilter, Clubs, Gender, Details
from rest_framework import serializers

# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'is_staff']

class ActivityFilterSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        models = ActivityFilter
        fields = '__all__'

class ActivitySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        models = Activity
        fields = '__all__'

class ClubsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        models = Clubs
        fields = '__all__'

class GenderSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        models = Gender
        fields = '__all__'

class DetailsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        models = Details
        fields = '__all__'