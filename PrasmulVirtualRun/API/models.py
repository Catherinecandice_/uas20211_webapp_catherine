from django.db import models
from django.db.models.deletion import CASCADE

# Create your models here.
class ActivityFilter(models.Model):
    activity_filter = (
    ('running','RUNNING'),
    ('ride','RIDE'),
    ('cycling','CYCLING'),
    ('all','ALL')
    )
    my_field = models.CharField(max_length=7, choices=activity_filter, default='running', null=True)
        
    def __str__(self):
        return str(self.activity_filter)

class Activity(models.Model):
    id_activity=models.IntegerField(null=True)
    kudos_count=models.IntegerField(null=True)
    distance=models.IntegerField(null=True)
    suffer_score=models.IntegerField(null=True)
    max_heartrate=models.IntegerField(null=True)
    total_elevation_gain=models.IntegerField(null=True)
    activity_choice=models.ForeignKey(ActivityFilter, on_delete=models.CASCADE)
    type=models.CharField(max_length=100)

    def __str__(self):
        return str(self.kudos_count)

class Clubs(models.Model):
    id_clubs=models.IntegerField(null=True)
    name=models.CharField(max_length=300, null=True)
    url=models.URLField
    profile_medium=models.CharField(max_length=300, null=True)

    def __str__(self):
        return str(self.id_clubs)

class Gender(models.Model):
    gender_choices = (
    ('male','MALE'),
    ('female','FEMALE')
    )
    my_field = models.CharField(max_length=6, choices=gender_choices, default='male', null=True)
        
    def __str__(self):
        return str(self.gender)

class Details(models.Model):
    id_details=models.IntegerField(null=True)
    firstname=models.CharField(max_length=100, null=True)
    lastname=models.CharField(max_length=100, null=True)
    profile=models.CharField(max_length=300, null=True)
    chosen_clubs=models.ForeignKey(Clubs, on_delete=models.CASCADE)
    gender_choice=models.ForeignKey(Gender, on_delete=models.CASCADE)
    weight=models.IntegerField(null=True)
    city=models.CharField(max_length=100, null=True)
    state=models.CharField(max_length=100, null=True)
    created_at=models.CharField(max_length=100, null=True)

    def __str__(self):
        return str(self.id_details)
