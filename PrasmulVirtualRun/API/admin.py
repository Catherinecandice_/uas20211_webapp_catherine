from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Activity)
admin.site.register(models.ActivityFilter)
admin.site.register(models.Clubs)
admin.site.register(models.Gender)
admin.site.register(models.Details)
