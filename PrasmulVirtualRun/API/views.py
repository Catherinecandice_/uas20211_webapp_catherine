from django.shortcuts import render
from django.urls import path, include
from django.contrib.auth.models import User
from rest_framework import viewsets
from API.serializers import UserSerializer

# Create your views here. 
# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

